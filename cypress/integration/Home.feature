Feature: Home

  As a new client
  I want to select an electronic product to buy
  In order use it

    Scenario: Listed Products
      Given I am a new client
      When I reach the Home site
      Then I can see a list of products
    