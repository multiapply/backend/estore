// import { create } from "react-test-renderer";
import { render, screen } from '@testing-library/react';
import App from './App';

describe('App', () => {
  it('renders correctly', () => {
    const component = render(<App />);
    expect(component).toBeDefined();
  });
});
