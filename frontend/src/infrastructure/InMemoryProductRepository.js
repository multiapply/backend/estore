import products from '../data/products'

export class InMemoryProductRepository {
  search() {
    return products;
  }
}