import { render, screen } from '@testing-library/react';
import Home from '../Home';
import { BrowserRouter } from 'react-router-dom';

describe('Home', () => {
  it('renders correctly', () => {
    render(<Home />, {wrapper: BrowserRouter});
    
    const title = screen.getByText(/Latests Products/i)
    expect(title).toBeInTheDocument();
  });
});
