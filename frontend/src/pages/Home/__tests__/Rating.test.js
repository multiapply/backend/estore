import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import Rating from '../Rating/Rating';

describe('Rating', () => {
  it('renders 5 full stars correctly', () => {
    const starNumber = 5
    const { container } = render(<Rating value={starNumber} />, {wrapper: BrowserRouter});
    const fullStars = container.querySelectorAll('.fas.fa-star');
    expect(fullStars).toHaveLength(starNumber);
  });

  it('renders 3 full stars correctly', () => {
    const starNumber = 3
    const { container } = render(<Rating value={starNumber} />, {wrapper: BrowserRouter});
    const fullStars = container.querySelectorAll('.fas.fa-star');
    expect(fullStars).toHaveLength(starNumber);
  });

  it('renders 3.5 stars correctly', () => {
    const starNumber = 3.5
    const { container } = render(<Rating value={starNumber} />, {wrapper: BrowserRouter});
    const fullStars = container.querySelectorAll('.fas.fa-star');
    const halfStars = container.querySelectorAll('.fas.fa-star-half-alt');
    expect(fullStars).toHaveLength(Math.floor(starNumber));
    expect(halfStars).toHaveLength(1);
  });
});
