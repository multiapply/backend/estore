import { render, screen } from '@testing-library/react';
import Product from '../Product/Product';
import products from '../../../data/products';
import { BrowserRouter } from 'react-router-dom';

describe('Product', () => {
  it('renders correctly', () => {
    const firstProduct = products[0]
    const component = render(<Product product={firstProduct} />, {wrapper: BrowserRouter});
    expect(component).toBeDefined();
  });

  it('shows product name', () => {
    const firstProduct = products[0]
    const component = render(<Product product={firstProduct} />, {wrapper: BrowserRouter});
    const productName = screen.getByText(firstProduct.name)
    expect(productName).toBeInTheDocument();
  });
});
