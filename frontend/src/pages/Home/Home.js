import React, { useState, useEffect } from 'react'
import { Row, Col} from 'react-bootstrap'
import Product from './Product/Product'
import { InMemoryProductRepository } from '../../infrastructure/InMemoryProductRepository'

const Home = () => {
  const repository = new InMemoryProductRepository
  const repositories = repository.search();

  
  return (
    <>
      <h1>Latests Products</h1>
      <Row>
        {repositories.map(product => (
          <Col key={product._id} sm={12} md={6} lg={4} xl={3}>
            <Product product={product} />
          </Col>
        ))}
      </Row>
    </>
  )
}

export default Home