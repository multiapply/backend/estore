import React from 'react'
import PropTypes from 'prop-types'

import './Rating.css';

const Rating = ({ value, text, color}) => {

  function stars(rate, low,high) {
    if (rate >= high){
      return 'fas fa-star';
    }
    if (rate >= low) {
      return 'fas fa-star-half-alt';
    }
    return 'far fa-star'
  }

  return (
    <div className='rating' aria-label='Rating Stars'>
      <span>
        <i 
          style= {{color}}
          className={stars(value,0.5,1)}
        ></i>
      </span>
      <span>
        <i 
          style= {{color}}
          className={stars(value,1.5,2)}
        ></i>
      </span>
      <span>
        <i 
          style= {{color}}
          className={stars(value,2.5,3)}
        ></i>
      </span>
      <span>
        <i 
          style= {{color}}
          className={stars(value,3.5,4)}
        ></i>
      </span>
      <span>
        <i 
          style= {{color}}
          className={stars(value,4.5,5)}
        ></i>
      </span>

      <span>{text && text }</span>
    </div>
  )
}

Rating.defaultProps = {
  color: '#f8e825'
}

Rating.propTypes = {
  value: PropTypes.number.isRequired,
  text: PropTypes.string,
  color: PropTypes.string
}

export default Rating